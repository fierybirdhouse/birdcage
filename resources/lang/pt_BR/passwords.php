<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'A senha deve conter pelo menos seis caracteres e ser igual à de confirmação.',
    'reset' => 'Sua senha foi redefinida!',
    'sent' => 'Te enviamos por e-mail o link para redefinir sua senha!',
    'token' => 'Esse token para redefinição de senha é inválido.',
    'user' => "Não há usuários com este endereço de e-mail.",

];
