<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Senha ou e-mail inválido',
    'throttle' => 'Woah, ocorreram muitas tentativas de login. Descanse por :seconds segundos antes de tentar novamente.',

];
