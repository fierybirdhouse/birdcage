@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Registros</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div id="result"></div>

                    <center>
                        <div id="status"></div>
                        <button id='botaoChecar' class="ui grey disabled loading basic button"></button>
                        <div id="timer"></div>
                        <hr />
                        <div class="ui breadcrumb">
                            <span class="ui white ribbon label">
                                <i class="filter icon"></i>
                                Filtros
                            </span>
                            <div class="ui dropdown" id="dropdownUsers">
                                <i class="tags icon"></i>
                                <span class="text">Usuário</span>
                                <i class="dropdown icon"></i>
                                <div class="menu" id="menuUsers">
                                    <div class="header">Filtrar por usuário</div>
                                    <div class="item" data-value="">Todos</div>
                                </div>
                            </div>
                            <div class="divider">/</div>
                            <div class="ui dropdown" id="dropdownMes">
                                <i class="calendar icon"></i>
                                <span class="text">Mês</span>
                                <i class="dropdown icon"></i>
                                <div class="menu" id="menuMes">
                                    <div class="header">Filtrar por mês</div>
                                    <div class="item" data-value="">Todos</div>
                                </div>
                            </div>
                        </div>
                        <table class="ui very basic collapsing table">
                            <thead>
                                <tr>
                                    <th>Funcionário</th>
                                    <th>Entrada</th>
                                    <th>Saída</th>
                                    <th>Turno</th>
                                </tr>
                            </thead>
                            <tbody id="registro">
                            </tbody>
                        </table>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#dropdownUsers, #dropdownMes').dropdown({onChange: function(text, value) {
        filter();
    }});
    tick = null; // Armazena time interval
    changeView(null);
    createMonth();
});

function filter() {
    dadosUser = $('#dropdownUsers').dropdown("get values");
    dadosMes = $('#dropdownMes').dropdown("get values")

    var data = {_token: "{{ csrf_token() }}"};
    data['mes'] = typeof dadosMes === 'undefined' ? null : dadosMes;
    data['codUsuario'] = typeof dadosUser === 'undefined' ? null : dadosUser;

    updateTable(data);
};

function changeView(codHorario) {

    // Atualiza criação de novos registros e obtém o último check-in
    $.post("{{ route('gravarRegistro', '') }}/" + codHorario, {_token: "{{ csrf_token() }}"})
    .done(function(response) {
        // Atualiza codHorario do último check-in
        codHorario = response

        if (codHorario == 0) {
            $("#botaoChecar").html("Check-in").removeClass("disabled loading");
        } else {
            $("#botaoChecar").html("Check-out").removeClass("disabled loading");
            timer();
        }

        // Atualiza evento do botaoChecar com o novo codHorario
        $("#botaoChecar").unbind('click').click(function() {
            $(this).html('Carregando...').addClass("disabled loading");
            changeView(codHorario);
        });

        // Obtém lista de registros
        $.post("{{ route('getRegistros') }}", {_token: "{{ csrf_token() }}"})
        .done(function(registros) {
            var user = '{{ Auth::user()->id }}'
            createTable(registros);

            timer(registros[findUser(registros, user)]['entrada'], parseInt(codHorario));
        });
        $.post("{{ route('getRegistros') }}", {_token: "{{ csrf_token() }}", listaUsuarios: true})
        .done(function(registros) {
            createUser(registros);
        });

    })
    .fail(function() {
        $("#botaoChecar").html("Tentar novamente").removeClass("disabled loading");
    });
}

function createMonth() {
    var months = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
    $(months).each(function(index) {
        var month = index+1
        $("#menuMes").append('<div class="item" data-value="' + month + '">' + months[index] + '</div>')
    });
}

function createUser(registers) {
    for (var index in registers) {
        $("#menuUsers").append('<div class="item" data-value="' + index + '">' + registers[index] + '</div>') 
    }
}

function updateTable(data) {
    $.post("{{ route('getRegistros') }}", data)
    .done(function(registros) {
        createTable(registros);
    });
}

function createTable(registros) {
    $("#registro").empty();
    
    function formatTime(timeStamp) {
        if (timeStamp !== '') {
            var time = new Date(timeStamp*1000);
            return pad(time.getHours()) + ":" + pad(time.getMinutes()) + ":" + pad(time.getSeconds()) + " (" + time.getDate() + "/" + time.getMonth() + ")";
        } else {
            return "--";
        }
    }

    function checkWork(entrada, saida) {
        if (saida !== '') {
            var turno = saida - entrada;
            return (Math.floor(turno/(60*60)%24) == 0 ? "" : pad(Math.floor(turno/(60*60)%24)) + ":") + pad(Math.floor(turno/60%60)) + ":" + pad(Math.floor(turno%60));
        } else {
            return "Trabalhando";
        }
    }

    $(registros).each(function(index) {
        $("#registro").append("<tr>" + "<td>" + registros[index]['usuario'] + "</td>" + "<td>" + formatTime(registros[index]['entrada']) + "</td>" + "<td>" + formatTime(registros[index]['saida']) + "</td>" + "<td>" + checkWork(registros[index]['entrada'], registros[index]['saida']) + "</td>" + "</tr>");
    });
}

function findUser(registers, codUser) {
    for (i = 0; i < registers.length; i++) {
            if (registers[i]['codUsuario'] == codUser) {
            return i;
        }
    }
}

function timer(inicio, enabled) {
    if (enabled && typeof inicio === 'number') {
        var agora = new Date()/1000;
        tick = setInterval(function() {
            agora++;
            var tempo = agora - inicio;
            H = pad(Math.floor(tempo/(60*60)%24));
            M = pad(Math.floor(tempo/60%60));
            S = pad(Math.floor(tempo%60));
            $("#timer").text(H + ":" + M + ":" + S);
        }, 1000);
    } else {
        clearInterval(tick);
        $("#timer").empty();
    }
}

function pad(n) {
    return String("0" + n).slice(-2);
}
</script>

@endsection

