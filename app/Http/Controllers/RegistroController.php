<?php

namespace birdcage\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use birdcage\Horario;

class RegistroController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function gravar($codHorario)
    {
        // Verifica se o usuário está autenticado ou se foi especificado
        $codUsuario = empty($_POST['codUsuario']) ? Auth::user()->id : $_POST['codUsuario'];

        if ($codHorario === "null") {
            // Procura pelo último registro sem check-out
            $ultimoRegistro = DB::table('Horarios')
            ->where('codUsuario', $codUsuario)
            ->whereNull('saida')
            ->get();

            // Se não houver registro sem check-out retornará 0
            $codHorario = empty($ultimoRegistro[0]) ? 0 : $ultimoRegistro[0]->codHorario;

            echo $codHorario;
        } elseif (is_numeric($codHorario) && !empty($codHorario) && $codHorario != 0) {
            // Calcula quando o limite será atingido (12 horas depois do check-in)
            $tempoLimite = strtotime(Horario::find($codHorario)->entrada) + 3600 * 12;

            // Usa o tempo limite de 12 horas caso esse período já tenha passado
            $tempoSaida = time() > $tempoLimite ? $tempoLimite : time();

            DB::table('Horarios')
                ->where('codHorario', $codHorario)
                ->update(['saida' => date('Y-m-d H:i:s', $tempoSaida)]);

            echo 0;
        } else {
            // Cria novo registro de check-in
            $proxCodHorario = DB::table('Horarios')->insertGetId([
                'codUsuario' => $codUsuario,
                'entrada' => date('Y-m-d H:i:s'),
                'saida' => null
            ]);

            echo $proxCodHorario;
        }
    }
}
