<?php

namespace birdcage\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use birdcage\Horario;
use birdcage\User;

class RegistroAPIController extends Controller
{
    public function __construct()
    {
        $discordId = empty($_POST['id']) ? '' : $_POST['id'];
        $idList = [
            'mts' => env('MTS_ID'),
            'asbel' => env('ASBEL_ID')
        ];

        // Se não foi informado (corretamente) o ID de nenhum dos dois
        if (!( (!empty($idList['mts']) && $discordId == $idList['mts']) || (!empty($idList['asbel']) && $discordId == $idList['asbel']) )) { 
            $this->middleware('auth');
        }
    }

    public function getAll() {
        // Busca lista de usuários
        if (!empty($_POST['listaUsuarios']) && $_POST['listaUsuarios']) {
            $pesquisa = User::all();
            $result = [];

            foreach ($pesquisa as $key => $row) {
                $result[$row->id] = $row->name;
            }

            unset($pesquisa);
        } else {
            // Busca lista de check-in/out filtrado

            $result = Horario::orderBy('entrada', 'desc');

            if (!empty($_POST['codUsuario'])) {
                $result = $result->where('codUsuario', $_POST['codUsuario']);
            }

            if (!empty($_POST['mes'])) {
                $result = $result->whereMonth('entrada', $_POST['mes']);
            }

            $result = $result->get();

            // Substitue codUsuario por nome e dateTime por data formatada
            foreach ($result as $key => $row) {
                // Substitue codUsuario por name
                $result[$key]->usuario = Horario::find($row->codUsuario)->usuario->name;

                // Substitue dateTime por timestamp
                $result[$key]->entrada = strtotime($result[$key]->entrada);

                // Dá um valor padrão e substitue dateTime por timestamp
                $result[$key]->saida = empty($result[$key]->saida) ? "" : strtotime($result[$key]->saida);
            }
        }

        return $result;
    }
}
