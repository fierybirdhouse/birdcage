<?php

namespace birdcage;

use Illuminate\Database\Eloquent\Model;

class Horario extends Model
{
    protected $primaryKey = 'codHorario';
    protected $table = 'Horarios';
    protected $fillable = array('codUsuario', 'entrada', 'saida');
    public $timestamps = false;

    public function usuario() {
        return $this->hasOne('birdcage\User', 'id', 'codUsuario');
    }
}
