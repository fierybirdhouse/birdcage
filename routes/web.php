<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Registration Routes...
if (!env('APP_REGISTRATION', false)) {
    Route::get('register', 'HomeController@index')->name('register');
    Route::post('register', 'HomeController@index');
}

Route::get('/home', 'HomeController@index')->name('home');
Route::match(['get', 'post'], '/registro/{codHorario}', 'RegistroController@gravar')->name('gravarRegistro');
Route::post('/registros', 'RegistroAPIController@getAll')->name('getRegistros');
